asnp citrix* -ErrorAction SilentlyContinue
$date = (Get-Date).ToString('yyyy-MM-dd')
$filepath1 = '\\corp.global.level3.com\dfs01\homeshares\IDC\Home5\das.tuhin\test\'+$date+'Good_servers1.csv'
$filepath2 = '\\corp.global.level3.com\dfs01\homeshares\IDC\Home5\das.tuhin\test\'+$date+'Bad_servers1.csv'
$machines = Get-BrokerMachine | where {($_.DesktopGroupName -notlike '*desktop - test*' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*desktop - oss adc' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*operations' -and $_.DesktopGroupName -notlike '*standalone' -and $_.DesktopGroupName -notlike '*metadata test' -and $_.DesktopGroupName -notlike ''-and $_.DesktopGroupName -notlike '*apm' -and $_.DesktopGroupName -notlike '*3scape'-and $_.DesktopGroupName -notlike '*nucleus'-and $_.DesktopGroupName -notlike '*tools dmz*' -and$_.DesktopGroupName -notlike '*Legacy adc') -and $_.RegistrationState -like '*unregistered*'} | select -Property MachineName, SessionCount

foreach($machine in $machines)
{
    Write-Host $machine.MachineName 'put in maintenance'
    Set-BrokerMachineMaintenanceMode -InputObject $machine.MachineName $true
}
$unregmac = Get-BrokerMachine | where {($_.DesktopGroupName -notlike '*desktop - test*' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*desktop - oss adc' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*operations' -and $_.DesktopGroupName -notlike '*standalone' -and $_.DesktopGroupName -notlike '*metadata test' -and $_.DesktopGroupName -notlike ''-and $_.DesktopGroupName -notlike '*apm' -and $_.DesktopGroupName -notlike '*3scape'-and $_.DesktopGroupName -notlike '*nucleus'-and $_.DesktopGroupName -notlike '*tools dmz*'-and$_.DesktopGroupName -notlike '*Legacy adc' ) -and $_.InMaintenanceMode -like 'true'} | select -Property MachineName, SessionCount
if($unregmac -eq $null)
    {Write-host 'all Servers are registered'}
else{

foreach($unreregmac1 in $unregmac)
{
    if($unreregmac1.SessionCount -eq 0 -or $unreregmac1.SessionCount -eq $null)
    {
        Write-Host $unreregmac1.MachineName 'eligible for will reboot session is'$unreregmac1.SessionCount' session'
        #New-BrokerHostingPowerAction -MachineName $unreregmac1.MachineName -Action Reset
        #(rebooting machine)
    }
    else {
        Write-Host $unreregmac1.MachineName 'Server has sessions'
        }

}
}

#Start-Sleep -Seconds 2000

$regmac = Get-BrokerMachine | where {($_.DesktopGroupName -notlike '*desktop - test*' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*desktop - oss adc' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*operations' -and $_.DesktopGroupName -notlike '*standalone' -and $_.DesktopGroupName -notlike '*metadata test' -and $_.DesktopGroupName -notlike ''-and $_.DesktopGroupName -notlike '*apm' -and $_.DesktopGroupName -notlike '*3scape'-and $_.DesktopGroupName -notlike '*nucleus'-and $_.DesktopGroupName -notlike '*tools dmz*' ) -and $_.InMaintenanceMode -like 'true' -and $_.RegistrationState -like 'registered'} | select -Property MachineName
foreach ($reg in $regmac)
{
    #Set-BrokerMachineMaintenanceMode -InputObject $reg.MachineName $false
    Write-Host $reg.MachineName
}

$unregmac1 = Get-BrokerMachine | where {($_.DesktopGroupName -notlike '*desktop - test*' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*desktop - oss adc' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*operations' -and $_.DesktopGroupName -notlike '*standalone' -and $_.DesktopGroupName -notlike '*metadata test' -and $_.DesktopGroupName -notlike ''-and $_.DesktopGroupName -notlike '*apm' -and $_.DesktopGroupName -notlike '*3scape'-and $_.DesktopGroupName -notlike '*nucleus'-and $_.DesktopGroupName -notlike '*tools dmz*'-and$_.DesktopGroupName -notlike '*Legacy adc' ) -and $_.InMaintenanceMode -like 'true'}
$regmac1 = Get-BrokerMachine | where {($_.DesktopGroupName -notlike '*desktop - test*' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*desktop - oss adc' -and $_.DesktopGroupName -notlike '*desktop - dcn emea*' -and $_.DesktopGroupName -notlike '*operations' -and $_.DesktopGroupName -notlike '*standalone' -and $_.DesktopGroupName -notlike '*metadata test' -and $_.DesktopGroupName -notlike ''-and $_.DesktopGroupName -notlike '*apm' -and $_.DesktopGroupName -notlike '*3scape'-and $_.DesktopGroupName -notlike '*nucleus'-and $_.DesktopGroupName -notlike '*tools dmz*'-and$_.DesktopGroupName -notlike '*Legacy adc' ) -and $_.InMaintenanceMode -like 'false' -and $_.RegistrationState -like 'registered'}

$start_table = '<html>
<head>
<style>
table {
    font-family: , sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 4px;
}
h2 {border: 2px solid #000000;
    text-align:center ;
    padding: 5px;
    background-color:#D3D3D3
   }

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<h2 style="color:black">Servers Detail</h2>

<table>
  <tr>
    <th>Server Name</th>
    <th>Delivery Group</th>
    <th>Session Count</th>
    <th>Registration State</th>
    <th>Maintenance Mode</th>
    <th>C Drive Free Space</th>
    <th>D Drive Free Space</th>
  </tr>
  '

  $start_table0 = '<html>
<head>
<style>
table {
    font-family: , sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 4px;
}
h2 {border: 2px solid #000000;
    text-align:center ;
    padding: 5px;
    background-color:#D3D3D3
   }

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<h2 style="color:black">Servers Detail</h2>

<table>
  <tr>
    <th>Server Name</th>
    <th>Delivery Group</th>
    <th>Session Count</th>
    <th>Registration State</th>
    <th>Maintenance Mode</th>
    <th>Person Put InMaintenance</th>
    
  </tr>
  '

$table5 = '  <tr>
    <td>Name</th>
    <td>DG</th>
    <td>Count</td>
    <td>RS</td>
    <td>MM</td>
    <td style="background-color:#C_Color">CDrive</td>
    <td style="background-color:#D_Color">DDrive</td>
  </tr>

'
$table9 = '  <tr>
    <td>Name</th>
    <td>DG</th>
    <td>Count</td>
    <td>RS</td>
    <td>MM</td>
    <td>PPM</td>
  </tr>

'

$end_table1  = '
</table>
</body>
</html>'


$final1 = ""
$final2 = ''
$final3 = ''
$final4 = ''

foreach($i in $regmac1){
$Cdrive3 = Get-WmiObject �ComputerName $i.HostedMachineName �Class Win32_LogicalDisk | Where-Object{$_.DeviceID -like "C:"} | Select @{Label=�Free�;Expression={�{0:n3}� �F ($_.FreeSpace/$_.Size)}}
$Ddrive3 = Get-WmiObject �ComputerName $i.HostedMachineName �Class Win32_LogicalDisk | Where-Object{$_.DeviceID -like "D:"} | Select @{Label=�Free�;Expression={�{0:n3}� �F ($_.FreeSpace/$_.Size)}}

$Cdrive1 = Get-WmiObject �ComputerName $i.HostedMachineName �Class Win32_LogicalDisk | Where-Object{$_.DeviceID -like "C:"} | Select @{Label=�Free�;Expression={�{0:P2}� �F ($_.FreeSpace/$_.Size)}}
$Ddrive1 = Get-WmiObject �ComputerName $i.HostedMachineName �Class Win32_LogicalDisk | Where-Object{$_.DeviceID -like "D:"} | Select @{Label=�Free�;Expression={�{0:P2}� �F ($_.FreeSpace/$_.Size)}}
if(($Ddrive3.free -le 0.1 -and $Ddrive3.free -notlike $null)-or ($Cdrive3.Free -le 0.1 -and $Cdrive3.free -notlike $null)){
Set-BrokerMachineMaintenanceMode -InputObject $i.MachineName $true
$disk_table = $table5 -replace 'CDrive', $Cdrive1.Free
$disk_table = $disk_table -replace 'DDrive', $Ddrive1.Free
$disk_table = $disk_table -replace 'Name', $i.HostedMachineName
$disk_table = $disk_table -replace 'Count', $i.SessionCount
$disk_table = $disk_table -replace 'DG', $i.DesktopGroupName
if($Cdrive3.free -le 0.1 -and $Cdrive3.Free -notlike $null){
$disk_table = $disk_table -replace "#C_Color", "red"}
if($Ddrive3.free -le 0.1 -and $Ddrive3.Free -notlike $null){
$disk_table = $disk_table -replace "#D_Color", "red"}
if($Cdrive1.Free -gt 15 -and $Cdrive1.Free -notlike $null){
$disk_table = $disk_table -replace "#C_Color", "green"}
if($Ddrive1.Free -gt 15 -and $Ddrive1.Free -notlike $null){
$disk_table = $disk_table -replace "#D_Color", "green"}
if($Cdrive1.free -le 15 -and $Cdrive1.Free -gt 10 -and $Cdrive1.Free -notlike $null){
$disk_table = $disk_table -replace "#C_Color", "gold"}
if($Ddrive1.free -le 15 -and $Ddrive1.Free -gt 10 -and $Ddrive1.Free -notlike $null){
$disk_table = $disk_table -replace "#D_Color", "gold"}
$disk_table = $disk_table -replace 'RS', $i.RegistrationState
$disk_table = $disk_table -replace 'MM', 'True'
$final3 += $disk_table}




if (($Cdrive1.free -le 15 -and $Cdrive1.Free -gt 10 -and $Cdrive1.Free -notlike $null) -or ($Ddrive1.free -le 15 -and $Ddrive1.Free -gt 10 -and $Ddrive1.Free -notlike $null)) { 
Set-BrokerMachineMaintenanceMode -InputObject $i.MachineName $true
$d_table = $table5 -replace 'DDrive', $Ddrive1.Free
$d_table = $d_table -replace 'CDrive', $Cdrive1.Free
$d_table = $d_table -replace 'Name', $i.HostedMachineName
$d_table = $d_table -replace 'Count', $i.SessionCount
$d_table = $d_table -replace 'DG', $i.DesktopGroupName
$d_table = $d_table -replace 'RS', $i.RegistrationState
$d_table = $d_table -replace 'MM', $i.InMaintenanceMode
if($Cdrive1.Free -gt 15 -and $Cdrive1.Free -notlike $null){
$d_table = $d_table -replace "#C_Color", "green"}
if($Ddrive1.Free -gt 15 -and $Ddrive1.Free -notlike $null){
$d_table = $d_table -replace "#D_Color", "green"}
if($Cdrive1.free -le 15 -and $Cdrive1.Free -gt 10 -and $Cdrive1.Free -notlike $null){
$d_table = $d_table -replace "#C_Color", "gold"}
if($Ddrive1.free -le 15 -and $Ddrive1.Free -gt 10 -and $Ddrive1.Free -notlike $null){
$d_table = $d_table -replace "#D_Color", "gold"}
if($Cdrive3.free -le 0.1 -and $Cdrive3.Free -notlike $null){
$d_table = $d_table -replace "#C_Color", "red"}
if($Ddrive3.free -le 0.1 -and $Ddrive3.Free -notlike $null){
$d_table = $d_table -replace "#D_Color", "red"}
$final4+= $d_table
}
else{
$good_table = $table5 -replace 'DDrive', $Ddrive1.Free
$good_table = $good_table -replace 'CDrive', $Cdrive1.Free
$good_table = $good_table -replace 'Name', $i.HostedMachineName
$good_table = $good_table -replace 'Count', $i.SessionCount
$good_table = $good_table -replace 'DG', $i.DesktopGroupName
$good_table = $good_table -replace 'RS', $i.RegistrationState
$good_table = $good_table -replace 'MM', $i.InMaintenanceMode
if($Cdrive1.Free -gt 15 -and $Cdrive1.Free -notlike $null){
$good_table = $good_table -replace "#C_Color", "green"}
if($Ddrive1.Free -gt 15 -and $Ddrive1.Free -notlike $null){
$good_table = $good_table -replace "#D_Color", "green"}
if($Cdrive1.free -le 15 -and $Cdrive1.Free -gt 10 -and $Cdrive1.Free -notlike $null){
$good_table = $good_table -replace "#C_Color", "gold"}
if($Ddrive1.free -le 15 -and $Ddrive1.Free -gt 10 -and $Ddrive1.Free -notlike $null){
$good_table = $good_table -replace "#D_Color", "gold"}
if($Cdrive3.free -le 0.1 -and $Cdrive3.Free -notlike $null){
$good_table = $good_table -replace "#C_Color", "red"}
if($Ddrive3.free -le 0.1 -and $Ddrive3.Free -notlike $null){
$good_table = $good_table -replace "#D_Color", "red"}

$final1 += $good_table}

}
$Date = Get-Date
$StartDate = $Date.AddDays(-90) 
$EndDate = $Date
$LogEntrys = Get-LogLowLevelOperation -MaxRecordCount 5000 -Filter { StartTime -ge $StartDate -and EndTime -le $EndDate } | Where { ($_.Details.PropertyName -eq 'MAINTENANCEMODE') -and ($_.Details.NewValue -eq $True) } | Sort EndTime -Descending
foreach( $i in $unregmac1){
$objMaintenance = $LogEntrys | Where { $_.Details.TargetName.ToUpper() -match [regex]::escape($i.MachineName) } | Select -First 1

$unreg_table = $table9 -replace 'PPM', $objMaintenance.user
$unreg_table = $unreg_table -replace 'Name', $i.HostedMachineName
$unreg_table = $unreg_table -replace 'Count', $i.SessionCount
$unreg_table = $unreg_table -replace 'DG', $i.DesktopGroupName
$unreg_table = $unreg_table -replace 'RS', $i.RegistrationState
$unreg_table = $unreg_table -replace 'MM', $i.InMaintenanceMode
$final2 += $unreg_table



}
$start_table1 = $start_table -replace 'Servers Detail', 'Servers Disk Space below 15%'
$start_table2 = $start_table0 -replace 'Servers Detail', 'Server in Maintenance/Required restart'
$start_table3 = $start_table -replace 'Servers Detail', 'Servers Disk Space below 10%'

$table1 = $start_table1 + $final4 + $end_table1
$table2 = $Start_table2 +$final2 + $end_table1
$table7 = $Start_table3 +$final3 + $end_table1
$table8 = $Start_table +$final1 + $end_table1

$smtpServer = "10.1.117.70"

$fromaddress = "citrixteam@level3.com" 
$toaddress1 = "citrixteam@level3.com"

  
$Subject = "Citrix Desktop Prod Check " 
$Body = '
Hi All,

<br>Please find the Servers detail for '+$date+'

<br> <br>Servers those needs to reboot (unregistered or scheduled to reboot ) <br>
'+$table2+'
<br> <br> Servers those are having disk-space less than 15%<br>
'+$table1+'<br>Servers those are having disk-space less than 10%<br>'+$table7+'<br><br>



<br> <br> Thanks, <br> Citrix Team.'
 
#################################### 
 
$message = new-object System.Net.Mail.MailMessage 
$message.From = $fromaddress 
$message.To.Add($toaddress1) 

$message.IsBodyHtml = $True 
$message.Subject = $Subject 
$smtp = new-object Net.Mail.SmtpClient($smtpServer) 
$credentials=new-object system.net.networkcredential(�user�,�password�)
$smtp.credentials=$credentials.getcredential($smtpServer,25,�basic�)
$message.body = $body 
$smtp.Send($message) 